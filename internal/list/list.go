package list

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/ReneKroon/ttlcache/v2"
	"gitlab.com/mldns/webservice/internal/model"
	"gitlab.com/mldns/webservice/internal/util"
)

const LIST_DIR = "lists"

func getListName(status model.Status) string {
	switch status {
	case model.Blocked:
		return "Blocklist"
	case model.Allowed:
		return "Allowlist"
	default:
		return "Unknown List"
	}
}

func CreateFile(filename string) {
	os.Mkdir(LIST_DIR, 0777)
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	f.Close()
}

// Create all list Files
func CreateFiles() {
	CreateFile(getFileName(model.Blocked, model.Personal))
	CreateFile(getFileName(model.Allowed, model.Personal))
	CreateFile(getFileName(model.Blocked, model.Provider))
	CreateFile(getFileName(model.Allowed, model.Provider))
	CreateFile(getFileName(model.Blocked, model.MLDNS))
	CreateFile(getFileName(model.Allowed, model.MLDNS))
}

func LoadListIntoCache(c *ttlcache.Cache, level model.Level, status model.Status) {
	fileName := getFileName(status, level)
	oldestTTL := time.Now().Add(-util.GetTTLDuration())
	records, _ := LoadList(fileName)
	for _, record := range records {
		if level == model.MLDNS {
			recordTime, _ := time.Parse(time.UnixDate, record[1])
			if recordTime.Before(oldestTTL) {
				RemoveFromFile(model.Domain{Domain: record[0], Level: level, Status: status})
			} else {
				ttl := util.GetTTLDuration() - time.Since(recordTime)
				c.SetWithTTL(record[0], status, ttl)
			}
		} else {
			c.Set(record[0], status)
		}
	}
}

// Load the list according to status and level from file und return it as slice of struct Domain
func GetList(status model.Status, level model.Level) (list []model.Domain, err error) {
	fileName := getFileName(status, level)
	records, err := LoadList(fileName)
	if err != nil {
		return nil, err
	}

	for _, record := range records {
		timestamp, _ := time.Parse(time.UnixDate, record[1])
		list = append(list, model.Domain{Domain: record[0], Timestamp: timestamp, Level: level, Status: status})
	}

	return list, nil

}

// Load list from file and return it as string map
func LoadList(fileName string) (records [][]string, err error) {
	file, err := os.Open(fileName)
	if err != nil {
		log.Println(err)
	}
	reader := csv.NewReader(file)
	return reader.ReadAll()
}

// Check if a domain is already written in a file
func checkDomainInList(fileName string, domain string) bool {
	// Check if already in there
	records, _ := LoadList(fileName)
	for _, record := range records {
		if record[0] == domain {
			return true
		}
	}
	return false
}

// Append a domain to a file. File is chosen by level and status of domain.
func WriteToFile(domain model.Domain) {
	fileName := getFileNameByDomain(domain)

	// if already in List skip writing
	if checkDomainInList(fileName, domain.Domain) {
		return
	}

	f, e := os.OpenFile(fileName, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if e != nil {
		log.Println(e)
	}
	writer := csv.NewWriter(f)

	record := []string{domain.Domain, domain.Timestamp.Format(time.UnixDate)}
	writer.Write(record)

	writer.Flush()
}

func getFileNameByDomain(domain model.Domain) string {
	return getFileName(domain.Status, domain.Level)
}

func getFileName(status model.Status, level model.Level) string {
	fileName := LIST_DIR + "/"
	if status == model.Blocked {
		fileName += "blacklist"
	} else {
		fileName += "whitelist"
	}

	switch level {
	case model.Personal:
		fileName += "_personal"
	case model.Provider:
		fileName += "_provider"
	case model.MLDNS:
		fileName += "_mldns"
	default:
		fileName += "_personal"
	}

	fileName += ".conf"

	return fileName
}

// Remove a domain from the corresponding file if it exists
func RemoveFromFile(domainItem model.Domain) {
	fileName := getFileNameByDomain(domainItem)

	records, _ := LoadList(fileName)

	for index, record := range records {
		if record[0] == domainItem.Domain {
			log.Printf("Found in %s: %s\n", fileName, record[0])

			records = append(records[:index], records[index+1:]...)
			f, e := os.OpenFile(fileName, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
			if e != nil {
				log.Println(e)
			}
			writer := csv.NewWriter(f)
			writer.WriteAll(records)
			return
		}
	}
	fmt.Printf("Not found in %s: %s\n", fileName, domainItem.Domain)

}

// Check if a domain is in the in-memory list. Checks als the possible wildcard domains for the given domains
func CheckBlocked(domain string, c *ttlcache.Cache) (model.Status, error) {
	for {
		if value, err := c.Get(domain); err == nil {
			fmt.Printf("%s blocked: %v\n", domain, value)
			return value.(model.Status), nil
		} else {
			var toShort bool
			domain, toShort = util.ChangeLowestSubdomainToWildcard(domain)

			if toShort {
				return model.Unknown, err
			}
		}
	}
}

func CheckAllLists(domain string, cPersonal *ttlcache.Cache, cProvider *ttlcache.Cache, cMLDNS *ttlcache.Cache) (model.Status, model.Level, error) {
	status, err := CheckBlocked(domain, cPersonal)
	if err == nil {
		return status, model.Personal, nil
	}

	status, err = CheckBlocked(domain, cProvider)
	if err == nil {
		return status, model.Provider, nil
	}

	status, err = CheckBlocked(domain, cMLDNS)
	if err == nil {
		return status, model.MLDNS, nil
	}

	return model.Unknown, model.MLDNS, nil
}

func GetListAsHTML(status model.Status, level model.Level) (string, error) {
	list, err := GetList(status, level)
	var table string

	if err != nil {
		return "", err
	}

	if len(list) == 0 {
		table += `<tr><td colspan="3">`
		table += "No data available"
		table += "</td></tr>"
	}

	for _, record := range list {
		table += "<tr><td>"
		table += record.Domain
		table += "</td><td>"
		table += record.Timestamp.String()
		table += `</td><td><div class="small-button-area">`
		if record.Status == model.Blocked {
			table += `<button class="ml-button small" onclick="addToList('`
			table += record.Domain + "'," + fmt.Sprint(model.Personal) + "," + fmt.Sprint(model.Allowed) + ",'" + getListName(model.Allowed) + "'"
			table += `)" >Allow</button>`
		} else {
			table += `<button class="ml-button small" onclick="addToList('`
			table += record.Domain + "'," + fmt.Sprint(model.Personal) + "," + fmt.Sprint(model.Blocked) + ",'" + getListName(model.Blocked) + "'"
			table += `)" >Block</button>`
		}
		table += `<button class="ml-button small" onClick="deleteFromList('`
		table += record.Domain + "'," + fmt.Sprint(record.Level) + "," + fmt.Sprint(record.Status) + ",'" + getListName(record.Status) + "'"
		table += `)" ><i class="fa fa-trash"></i></button></div> </td>`
		table += "</tr>"
	}

	return table, nil
}
