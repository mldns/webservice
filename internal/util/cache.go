package util

import (
	"net/http"

	"github.com/ReneKroon/ttlcache/v2"
)

// Add one in-memory list to http func
func AddCache(fn func(http.ResponseWriter, *http.Request, *ttlcache.Cache), c *ttlcache.Cache) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fn(w, r, c)
	}
}

// Add all three in-memory list to http func
func AddCaches(fn func(http.ResponseWriter, *http.Request, *ttlcache.Cache, *ttlcache.Cache, *ttlcache.Cache), cPersonal *ttlcache.Cache, cProvider *ttlcache.Cache, cMLDNS *ttlcache.Cache) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fn(w, r, cPersonal, cProvider, cMLDNS)
	}
}
