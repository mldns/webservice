package util

import (
	"encoding/json"
	"errors"
	"io"
	"strings"

	"gitlab.com/mldns/webservice/internal/model"
)

func SplitDomain(domain string) (splittedDomain []string) {
	return strings.Split(domain, ".")
}

func CombineDomain(splittedDomain []string) (domain string) {
	return strings.Join(splittedDomain, ".")
}

// Returns a domain where the lowest subdomain ist replaced by a '*'. Not done for first and second-level domain
func ChangeLowestSubdomainToWildcard(domain string) (wildcardDomain string, toShort bool) {

	splittedDomain := SplitDomain(domain)

	// termination condition (wildcard with TLD does not make sense for our usecase)
	if len(splittedDomain) <= 2 {
		return "", true
	}

	// If first subdomain is wildcard, trim of first subdomain
	if splittedDomain[0] == "*" {
		splittedDomain = splittedDomain[1:]
	}

	// Change first subdomain to wildcard
	splittedDomain[0] = "*"

	// termination condition (wildcard with TLD does not make sense for our usecase)
	toShort = len(splittedDomain) <= 2

	return CombineDomain(splittedDomain), toShort
}

// Trim the domain to have consistent domains.
// unbound domains have a trailing '.'
func PrepareDomain(domain string) string {
	return strings.TrimRight(domain, ".")
}

func UnmarshalDomain(body io.ReadCloser) (model.Domain, error) {
	var domainItem model.Domain
	var unmarshalErr *json.UnmarshalTypeError

	decoder := json.NewDecoder(body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&domainItem)
	if err != nil {
		if errors.As(err, &unmarshalErr) {
			return domainItem, errors.New("Wrong Type provided for field " + unmarshalErr.Field)
		} else {
			return domainItem, errors.New(err.Error())
		}
	}

	if domainItem.Status == model.Unknown {
		return domainItem, errors.New("Can't add domain with status 'unknown'")
	}

	domainItem.Domain = PrepareDomain(domainItem.Domain)
	return domainItem, nil
}
