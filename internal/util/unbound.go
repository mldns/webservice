package util

import (
	"fmt"
	"log"
	"net"
	"os"
	"os/exec"
)

var port = os.Getenv("UNBOUND_PORT")

func ReloadUnbound() {
	execCommand("reload")
}

func StartUnbound() {
	execCommand("start")
}

func StopUnbound() {
	execCommand("stop")
}

func StatsUnbound() {
	execCommand("stats_noreset")
}

func execCommand(command string) {
	ip := getIPFromDomain("unbound")

	execCommand := "unbound-control -s " + ip.String() + "@" + port + " " + command

	out, err := exec.Command("/bin/sh", "-c", execCommand).Output()

	if err != nil {
		log.Printf("%s", err)
	} else {
		log.Printf("unbound '%s' executed: %s", command, out)
	}
}

func getIPFromDomain(domain string) net.IP {
	ips, _ := net.LookupIP(domain)
	for _, ip := range ips {
		if ipv4 := ip.To4(); ipv4 != nil {
			fmt.Println("IPv4: ", ipv4)
			return ipv4
		}
	}
	return nil
}
