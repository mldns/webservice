package util

import (
	"log"
	"time"
)

func TimeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Printf("%s took %s", name, elapsed)
}

func Min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func GetTTLDuration() time.Duration {
	duration, _ := time.ParseDuration("24h")
	return duration
}

func GetAvg(times []float64) (int, float64) {
	// Total number of entries
	n := len(times)

	// calc avg processing time
	sum := 0.0
	for i := 0; i < n; i++ {
		sum += (times[i])
	}
	avg := (float64(sum)) / (float64(n))

	return n, avg
}
