package util

import "strings"

// Check if the file escapes from the current directory
func CheckFileName(fileName string) (valid bool) {
	if !strings.Contains(fileName, "..") {
		return false
	}
	for _, ent := range strings.FieldsFunc(fileName, isSlashRune) {
		if ent == ".." {
			return true
		}
	}
	return false
}

// Forward or backward slash
func isSlashRune(r rune) bool { return r == '/' || r == '\\' }

func CombineDirAndFile(baseDir string, fileName string) (filePath string, err error) {
	if CheckFileName(fileName) {
		return "", nil
	}

	return baseDir + "/" + fileName, nil
}
