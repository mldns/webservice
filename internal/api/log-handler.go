package api

import (
	"bufio"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/mldns/webservice/internal/model"
	"gitlab.com/mldns/webservice/internal/util"
)

// API endpoint to delete all logs gathered by unbound
func DeleteAllLogs(w http.ResponseWriter, r *http.Request) {
	logDir := os.Getenv("UNBOUND_BASE_DIR")
	filePath, err := util.CombineDirAndFile(logDir, "good.log")
	if err != nil {
		util.JsonResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}
	os.Remove(filePath)

	filePath, err = util.CombineDirAndFile(logDir, "bad.log")
	if err != nil {
		util.JsonResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}
	os.Remove(filePath)

	util.JsonResponse(w, "", http.StatusNoContent)
}

// API endpoint to fetch a specific log from unbound
func GetLog(w http.ResponseWriter, r *http.Request) {
	logName := mux.Vars(r)["log"]
	logDir := os.Getenv("UNBOUND_BASE_DIR")

	filePath, err := util.CombineDirAndFile(logDir, logName)
	if err != nil {
		return
	}

	http.ServeFile(w, r, filePath)
}

// API endpoint to delete a specific log file gathered by unbound
func DeleteLog(w http.ResponseWriter, r *http.Request) {
	logName := mux.Vars(r)["log"]
	logDir := os.Getenv("UNBOUND_BASE_DIR")
	filePath, err := util.CombineDirAndFile(logDir, logName)
	if err != nil {
		return
	}
	os.Remove(filePath)

	util.JsonResponse(w, "", http.StatusNoContent)
}

func ParseLog(w http.ResponseWriter, r *http.Request) {

	logName := mux.Vars(r)["log"]
	logDir := os.Getenv("UNBOUND_BASE_DIR")

	filePath, err := util.CombineDirAndFile(logDir, logName)
	if err != nil {
		return
	}

	if strings.Contains(logName, "time") {
		parseTimeLog(filePath, w)
	} else {
		parsePredictionLog(filePath, w)
	}

}

func parsePredictionLog(filePath string, w http.ResponseWriter) {

	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	times := []float64{}
	var types = map[string]int{}
	var domains = map[string]int{}
	resp := model.Stats{}

	scanner := bufio.NewScanner(file)
	// optionally, resize scanner's capacity for lines over 64K, see next example
	for scanner.Scan() {
		v := strings.Fields(scanner.Text())
		time, _ := strconv.ParseFloat(v[9], 64)
		times = append(times, time)
		types[v[7]] = types[v[7]] + 1
		domains[v[5]] = domains[v[5]] + 1
	}

	for queryType, count := range types {
		resp.Types = append(resp.Types, model.QType{TypeString: queryType, Count: count})
	}

	n, avg := util.GetAvg(times)
	resp.Times = append(resp.Times, model.TypeTimes{TypeString: "prediction", Count: n, Avg: avg, AvgUnit: "ns"})

	// build ranking for domain names
	// is only meaningful for good log
	// with bad every domain is only predicted once
	type kv struct {
		Key   string
		Value int
	}
	var ss []kv
	for k, v := range domains {
		ss = append(ss, kv{k, v})
	}

	sort.Slice(ss, func(i, j int) bool {
		return ss[i].Value > ss[j].Value
	})

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	// Check if at least 20 entries ar in ranking
	noEntries := util.Min(len(ss), 19)

	// Get top X entries
	for _, kv := range ss[0:noEntries] {
		resp.Ranking = append(resp.Ranking, model.DomainStat{Domain: kv.Key, Count: kv.Value})
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	jsonResp, _ := json.Marshal(resp)
	w.Write(jsonResp)
}

func parseTimeLog(filePath string, w http.ResponseWriter) {
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	timesHTTP := []float64{}
	timesGrpc := []float64{}
	timesWS := []float64{}

	var status = map[string]int{}
	resp := model.Stats{}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		v := strings.Fields(scanner.Text())
		typeString := v[4]
		time, _ := strconv.ParseFloat(v[6], 64)

		switch typeString {
		case "http":
			timesHTTP = append(timesHTTP, time)
			break
		case "grpc":
			timesGrpc = append(timesGrpc, time)
			break
		case "websocket":
			timesWS = append(timesWS, time)
			break
		}

		status[typeString] = status[typeString] + 1

	}

	nHTTP, avgHTTP := util.GetAvg(timesHTTP)
	resp.Times = append(resp.Times, model.TypeTimes{TypeString: "http", Count: nHTTP, Avg: avgHTTP, AvgUnit: "ms"})

	nGRPC, avgGRPC := util.GetAvg(timesGrpc)
	resp.Times = append(resp.Times, model.TypeTimes{TypeString: "grpc", Count: nGRPC, Avg: avgGRPC, AvgUnit: "ms"})
    
	nWS, avgWS := util.GetAvg(timesWS)
	resp.Times = append(resp.Times, model.TypeTimes{TypeString: "ws", Count: nWS, Avg: avgWS, AvgUnit: "ms"})

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	jsonResp, _ := json.Marshal(resp)
	w.Write(jsonResp)

}
