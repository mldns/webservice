package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gitlab.com/mldns/webservice/internal/model"
	"gitlab.com/mldns/webservice/internal/util"
)

// API endpoint to fetch the current running config for unbound
func GetConfig(w http.ResponseWriter, r *http.Request) {

	data, err := ioutil.ReadFile("config/config.json")
	if err != nil {
		util.JsonResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// json data
	var obj model.Config

	err = json.Unmarshal(data, &obj)
	if err != nil {
		fmt.Println("error:", err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(obj)
}

func GetCert(w http.ResponseWriter, r *http.Request) {
	filePath, err := util.CombineDirAndFile(os.Getenv("UNBOUND_CERT_DIR"), "unbound_server.pem")
	if err != nil {
		log.Fatal("Error loading tls pem file")
		return
	}

	http.ServeFile(w, r, filePath)
}

// API endpoint to receive the config and apply it to unbound
func SetConfig(w http.ResponseWriter, r *http.Request) {

	headerContentType := r.Header.Get("Content-Type")
	if headerContentType != "application/json" {
		util.JsonResponse(w, "Content Type is not application/json", http.StatusUnsupportedMediaType)
		return
	}
	var e model.Config
	var unmarshalErr *json.UnmarshalTypeError

	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&e)
	if err != nil {
		if errors.As(err, &unmarshalErr) {
			util.JsonResponse(w, "Bad Request. Wrong Type provided for field "+unmarshalErr.Field, http.StatusBadRequest)
		} else {
			util.JsonResponse(w, "Bad Request "+err.Error(), http.StatusBadRequest)
		}
		return
	}

	file, _ := json.MarshalIndent(e, "", " ")

	_ = ioutil.WriteFile("config/config.json", file, 0644)

	util.StopUnbound()
	util.StartUnbound()

	util.JsonResponse(w, "Success", http.StatusOK)

}
