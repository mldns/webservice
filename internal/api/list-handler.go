package api

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/ReneKroon/ttlcache/v2"
	"github.com/gorilla/mux"
	"gitlab.com/mldns/webservice/internal/list"
	"gitlab.com/mldns/webservice/internal/model"
	"gitlab.com/mldns/webservice/internal/util"
)

// API endpoint to receive a list depending on level and status in query or default personal allowlist
func GetList(w http.ResponseWriter, r *http.Request, c *ttlcache.Cache) {
	query := r.URL.Query()
	statusQuery, err := strconv.Atoi(query.Get("status"))
	status := model.Status(statusQuery)
	if err != nil {
		status = model.Allowed
	}

	levelQuery, err := strconv.Atoi(query.Get("level"))
	level := model.Level(levelQuery)
	if err != nil {
		level = model.Personal
	}

	list, err := list.GetList(status, level)
	if err != nil {
		fmt.Println("error:", err)
		util.JsonResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if len(list) == 0 {
		util.JsonResponse(w, "No Entries Found.", http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(list)
}

// API endpoint to add an entry to a list
func AddToList(w http.ResponseWriter, r *http.Request, cPersonal *ttlcache.Cache, cProvider *ttlcache.Cache, cMLDNS *ttlcache.Cache) {

	domainItem, err := util.UnmarshalDomain(r.Body)
	if err != nil {
		util.JsonResponse(w, "Bad Request "+err.Error(), http.StatusBadRequest)
		return
	}

	domainItem.Timestamp = time.Now()
	oldDomainItem := domainItem
	if domainItem.Status == model.Blocked {
		oldDomainItem.Status = model.Allowed
	} else {
		oldDomainItem.Status = model.Blocked
	}
	list.RemoveFromFile(oldDomainItem)
	list.WriteToFile(domainItem)
	switch domainItem.Level {
	case model.Personal:
		cPersonal.Set(domainItem.Domain, domainItem.Status)
	case model.Provider:
		cProvider.Set(domainItem.Domain, domainItem.Status)
	case model.MLDNS:
		cMLDNS.Set(domainItem.Domain, domainItem.Status)

	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(domainItem)

	// add captcha to website
}

// API endpoint to remove one entry from a list
func RemoveFromList(w http.ResponseWriter, r *http.Request, cPersonal *ttlcache.Cache, cProvider *ttlcache.Cache, cMLDNS *ttlcache.Cache) {

	domainItem, err := util.UnmarshalDomain(r.Body)
	if err != nil {
		util.JsonResponse(w, "Bad Request: "+err.Error(), http.StatusBadRequest)
		return
	}

	log.Println("Remove: " + domainItem.Domain)

	switch domainItem.Level {
	case model.Personal:
		err = cPersonal.Remove(domainItem.Domain)
	case model.Provider:
		err = cProvider.Remove(domainItem.Domain)
	case model.MLDNS:
		err = cMLDNS.Remove(domainItem.Domain)
	}
	list.RemoveFromFile(domainItem)
	if err != nil {
		util.JsonResponse(w, "Bad Request: "+err.Error(), http.StatusBadRequest)
		return
	}

	util.JsonResponse(w, "Success", http.StatusNoContent)
}

// API endpoint to clear allowlist/blocklist
func RemoveList(w http.ResponseWriter, r *http.Request, cPersonal *ttlcache.Cache, cProvider *ttlcache.Cache, cMLDNS *ttlcache.Cache) {
	query := r.URL.Query()
	statusQuery, err := strconv.Atoi(query.Get("status"))
	status := model.Status(statusQuery)
	if err != nil {
		status = model.Allowed
	}

	levelQuery, err := strconv.Atoi(query.Get("level"))
	level := model.Level(levelQuery)
	if err != nil {
		level = model.Personal
	}

	domainList, err := list.GetList(status, level)

	var c *ttlcache.Cache
	switch level {
	case model.Personal:
		c = cPersonal
	case model.Provider:
		c = cProvider
	case model.MLDNS:
		c = cMLDNS
	}

	if err != nil {
		util.JsonResponse(w, "Bad Request "+err.Error(), http.StatusBadRequest)
		return
	}
	for _, domainItem := range domainList {
		list.RemoveFromFile(domainItem)
		c.Remove(domainItem.Domain)
	}

	util.JsonResponse(w, "", http.StatusNoContent)
}

// API endpoint to check if a domain is blocked or allowed in any list
func CheckList(w http.ResponseWriter, r *http.Request, cPersonal *ttlcache.Cache, cProvider *ttlcache.Cache, cMLDNS *ttlcache.Cache) {
	defer util.TimeTrack(time.Now(), "checkList")
	domain := util.PrepareDomain(mux.Vars(r)["domain"])
	status, _, _ := list.CheckAllLists(domain, cPersonal, cProvider, cMLDNS)
	if status == model.Blocked {
		w.WriteHeader(http.StatusBadRequest)
	} else if status == model.Allowed {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
	fmt.Fprintf(w, "%d", status)
}
