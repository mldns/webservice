package web

import (
	"html/template"
	"log"
	"net/http"

	"gitlab.com/mldns/webservice/internal/list"
	"gitlab.com/mldns/webservice/internal/model"
	"gitlab.com/mldns/webservice/internal/util"
)

// Display the blocked page which is served to every request not tailored for any other handler
func GetBlockedPage(w http.ResponseWriter, r *http.Request) {
	data := model.Domain{
		Domain: util.PrepareDomain(r.Host),
	}
	parsedTemplate, _ := template.ParseFiles("templates/blocked.html")
	err := parsedTemplate.Execute(w, data)
	if err != nil {
		util.JsonResponse(w, err.Error(), http.StatusInternalServerError)
		log.Println("Error executing template :", err)
		return
	}
}

// display the basic config page for mldns
func DisplayConfig(w http.ResponseWriter, r *http.Request) {
	data := model.Domain{}
	parsedTemplate, _ := template.ParseFiles("templates/config.html")
	err := parsedTemplate.Execute(w, data)
	if err != nil {
		util.JsonResponse(w, err.Error(), http.StatusInternalServerError)
		log.Println("Error executing template :", err)
		return
	}
}

type EditListData struct {
	ListType string
	ListVerb string
	Level    model.Level
	Status   model.Status
	Table    template.HTML
}

// Display the config page, where you can edit the personal allowlist
func DisplayConfigAllowlist(w http.ResponseWriter, r *http.Request) {

	table, err := list.GetListAsHTML(model.Allowed, model.Personal)

	if err != nil {
		util.JsonResponse(w, err.Error(), http.StatusInternalServerError)
		log.Println("Error getting table :", err)
		return
	}

	data := EditListData{
		ListType: "Allowlist",
		ListVerb: "allowed",
		Level:    model.Personal,
		Status:   model.Allowed,
		Table:    template.HTML(table),
	}

	parsedTemplate, _ := template.ParseFiles("templates/editList.html")
	err = parsedTemplate.Execute(w, data)
	if err != nil {
		util.JsonResponse(w, err.Error(), http.StatusInternalServerError)
		log.Println("Error executing template :", err)
		return
	}
}

// Display the config page, where you can edit the personal blocklist
func DisplayConfigBlocklist(w http.ResponseWriter, r *http.Request) {
	table, err := list.GetListAsHTML(model.Blocked, model.Personal)

	if err != nil {
		util.JsonResponse(w, err.Error(), http.StatusInternalServerError)
		log.Println("Error getting table :", err)
		return
	}

	data := EditListData{
		ListType: "Blocklist",
		ListVerb: "blocked",
		Level:    model.Personal,
		Status:   model.Blocked,
		Table:    template.HTML(table),
	}

	parsedTemplate, _ := template.ParseFiles("templates/editList.html")
	err = parsedTemplate.Execute(w, data)
	if err != nil {
		util.JsonResponse(w, err.Error(), http.StatusInternalServerError)
		log.Println("Error executing template :", err)
		return
	}
}

// Display the config page, where you can edit the mldns blocklist
func DisplayMldnsBlocklist(w http.ResponseWriter, r *http.Request) {
	table, err := list.GetListAsHTML(model.Blocked, model.MLDNS)

	if err != nil {
		util.JsonResponse(w, err.Error(), http.StatusInternalServerError)
		log.Println("Error getting table :", err)
		return
	}

	data := EditListData{
		ListType: "MLDNS Blocklist",
		ListVerb: "blocked by AI",
		Level:    model.MLDNS,
		Status:   model.Blocked,
		Table:    template.HTML(table),
	}

	parsedTemplate, _ := template.ParseFiles("templates/editList.html")
	err = parsedTemplate.Execute(w, data)
	if err != nil {
		util.JsonResponse(w, err.Error(), http.StatusInternalServerError)
		log.Println("Error executing template :", err)
		return
	}
}
