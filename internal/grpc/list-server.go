package grpc

import (
	context "context"
	"time"

	"github.com/ReneKroon/ttlcache/v2"
	"gitlab.com/mldns/webservice/internal/list"
	"gitlab.com/mldns/webservice/internal/model"
	"gitlab.com/mldns/webservice/internal/util"
)

type ListServer struct {
	UnimplementedListManagementServer
	CPersonal *ttlcache.Cache
	CProvider *ttlcache.Cache
	CMLDNS    *ttlcache.Cache
}

func (s *ListServer) Check(ctx context.Context, in *DomainRequest) (*DomainResponse, error) {
	defer util.TimeTrack(time.Now(), "checkGrpc")
	domain := util.PrepareDomain(in.Domain)

	response := &DomainResponse{Domain: in.Domain}

	status, level, _ := list.CheckAllLists(domain, s.CPersonal, s.CProvider, s.CMLDNS)
	response.Level = int32(level)
	response.Status = int32(status)
	return response, nil

}

func (s *ListServer) Insert(ctx context.Context, in *DomainRequest) (*DomainResponse, error) {
	domainItem := model.Domain{Domain: util.PrepareDomain(in.Domain), Level: model.Level(in.Level), Status: model.Status(in.Status), Timestamp: time.Now()}
	oldDomainItem := domainItem
	if domainItem.Status == model.Blocked {
		oldDomainItem.Status = model.Allowed
	} else {
		oldDomainItem.Status = model.Blocked
	}
	list.RemoveFromFile(oldDomainItem)
	list.WriteToFile(domainItem)
	switch domainItem.Level {
	case model.Personal:
		s.CPersonal.Set(domainItem.Domain, domainItem.Status)
	case model.Provider:
		s.CProvider.Set(domainItem.Domain, domainItem.Status)
	case model.MLDNS:
		s.CMLDNS.Set(domainItem.Domain, domainItem.Status)
	}

	response := &DomainResponse{Domain: domainItem.Domain, Level: in.Level, Status: in.Status}

	return response, nil
}
