package model

// User for login.
type User struct {
	Username          string `json:"username"`
	Password          string `json:"password,omitempty"`
	NotificationToken string `json:"notificationToken"`
}
