package model

// Config for unbound.
type Config struct {
	Active        bool    `json:"active"`
	Threshold     float32 `json:"threshold"`
	AnonymousData bool    `json:"anonymous_data"`
}
