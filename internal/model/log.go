package model

type Stats struct {
	Types   []QType      `json:"types,omitempty"`
	Ranking []DomainStat `json:"ranking,omitempty"`
	Times   []TypeTimes  `json:"times,omitempty"`
}

type QType struct {
	TypeString string `json:"type"`
	Count      int    `json:"count"`
}

type DomainStat struct {
	Domain string `json:"domain"`
	Count  int    `json:"count"`
}

type TypeTimes struct {
	TypeString string  `json:"type"`
	Count      int     `json:"count"`
	Avg        float64 `json:"avg"`
	AvgUnit    string  `json:"avgUnit"`
}
