package model

import "time"

type Domain struct {
	Domain    string    `json:"domain,omitempty"`
	Timestamp time.Time `json:"timestamp,omitempty"`
	Status    Status    `json:"status,omitempty"`
	Level     Level     `json:"level,omitempty"`
}

// status of the domain entry in list
type Status int

const (
	Blocked Status = iota + 1
	Allowed
	Unknown
)

// level of the list
type Level int

const (
	Personal Level = iota + 1
	Provider
	MLDNS
)
