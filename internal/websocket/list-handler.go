package websocket

import (
	"log"
	"net/http"
	"time"

	"github.com/ReneKroon/ttlcache/v2"
	"gitlab.com/mldns/webservice/internal/list"
	"gitlab.com/mldns/webservice/internal/model"
	"gitlab.com/mldns/webservice/internal/util"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{} // use default options

// API endpoint to check if a domain is blocked or allowed in any list
func CheckWs(w http.ResponseWriter, r *http.Request, cPersonal *ttlcache.Cache, cProvider *ttlcache.Cache, cMLDNS *ttlcache.Cache) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()

	for {
		mt, message, err := c.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			break
		}
		start := time.Now()
		domain := util.PrepareDomain(string(message))

		status, _, _ := list.CheckAllLists(domain, cPersonal, cProvider, cMLDNS)
		if status == model.Blocked {
			c.WriteMessage(mt, []byte("1"))
		} else if status == model.Allowed {
			c.WriteMessage(mt, []byte("2"))
		} else {
			c.WriteMessage(mt, []byte("3"))
		}
		elapsed := time.Since(start)
		log.Printf("%s took %s", "CheckWs", elapsed)
		if err != nil {
			log.Println("write:", err)
			break
		}

	}
}
