package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"sync"
	"time"

	"github.com/ReneKroon/ttlcache/v2"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"gitlab.com/mldns/webservice/internal/api"
	"gitlab.com/mldns/webservice/internal/list"
	"gitlab.com/mldns/webservice/internal/model"
	"gitlab.com/mldns/webservice/internal/util"
	"gitlab.com/mldns/webservice/internal/web"
	"gitlab.com/mldns/webservice/internal/websocket"

	pb "gitlab.com/mldns/webservice/internal/grpc"
	"google.golang.org/grpc"
)

func main() {
	initEnv()
	cPersonal := createCache(model.Personal)
	cProvider := createCache(model.Provider)
	cMLDNS := createCache(model.MLDNS)

	router := mux.NewRouter()

	// ===================== API ==========================
	apiRouter := router.PathPrefix("/api").Subrouter()

	listRouter := apiRouter.PathPrefix("/lists").Subrouter()
	listRouter.Path("").Methods(http.MethodDelete).HandlerFunc(util.AddCaches(api.RemoveList, cPersonal, cProvider, cMLDNS))
	listRouter.Path("").Methods(http.MethodPost).HandlerFunc(util.AddCaches(api.AddToList, cPersonal, cProvider, cMLDNS))
	listRouter.Path("").Methods(http.MethodGet).HandlerFunc(util.AddCache(api.GetList, cPersonal))
	listRouter.Path("/{domain}").Methods(http.MethodGet).HandlerFunc(util.AddCaches(api.CheckList, cPersonal, cProvider, cMLDNS))
	listRouter.Path("/{domain}").Methods(http.MethodDelete).HandlerFunc(util.AddCaches(api.RemoveFromList, cPersonal, cProvider, cMLDNS))

	logRouter := apiRouter.PathPrefix("/logs").Subrouter()
	logRouter.Path("").Methods(http.MethodDelete).HandlerFunc(api.DeleteAllLogs)
	logRouter.Path("/{log}").Methods(http.MethodDelete).HandlerFunc(api.DeleteLog)
	logRouter.Path("/{log}").Methods(http.MethodGet).HandlerFunc(api.GetLog)
	logRouter.Path("/{log}/stats").Methods(http.MethodGet).HandlerFunc(api.ParseLog)

	configRouter := apiRouter.PathPrefix("/config").Subrouter()
	configRouter.Path("").Methods(http.MethodGet).HandlerFunc(api.GetConfig)
	configRouter.Path("").Methods(http.MethodPost).HandlerFunc(api.SetConfig)
	configRouter.Path("/tls-cert").Methods(http.MethodGet).HandlerFunc(api.GetCert)

	// =========================== Static Assets ================================
	router.PathPrefix("/assets/").Handler(http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets"))))

	// ==================== WEB ==========================
	router.HandleFunc("/config", web.DisplayConfig)
	router.HandleFunc("/config/allowlist", web.DisplayConfigAllowlist)
	router.HandleFunc("/config/blocklist", web.DisplayConfigBlocklist)
	router.HandleFunc("/config/blocklist/mldns", web.DisplayMldnsBlocklist)

	// Get all not handled url and serve the block page
	router.HandleFunc(`/{rest:[a-zA-Z0-9=\-\/]+}`, web.GetBlockedPage)
	router.HandleFunc(`/`, web.GetBlockedPage)

	wg := new(sync.WaitGroup)
	wg.Add(3)

	go func() {
		log.Println("Serving on Port 80")
		log.Fatal(http.ListenAndServe(":80", router))
		wg.Done()
	}()

	go func() {
		router := mux.NewRouter()
		router.Path("/api/lists/ws").Methods(http.MethodGet).HandlerFunc(util.AddCaches(websocket.CheckWs, cPersonal, cProvider, cMLDNS))
		log.Println("Serving Websockets on Port 8081")
		log.Fatal(http.ListenAndServe(":8081", router))
		wg.Done()
	}()

	go func() {
		flag.Parse()
		lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 8080))
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}
		var opts []grpc.ServerOption
		grpcServer := grpc.NewServer(opts...)
		server := &pb.ListServer{CPersonal: cPersonal, CProvider: cProvider, CMLDNS: cMLDNS}
		pb.RegisterListManagementServer(grpcServer, server)
		log.Println("Serving gRPC on Port 8080")
		log.Fatal(grpcServer.Serve(lis))
		wg.Done()
	}()

	wg.Wait()

	// log.Fatal(http.ListenAndServeTLS(":443", "/home/leon/myCA.pem", "/home/leon/privkey.pem", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	// 	fmt.Printf("Host is %s\n", r.Host)
	// 	http.Redirect(w, r, "http://"+r.Host+r.RequestURI, http.StatusTemporaryRedirect)
	// })))
}

// Read .env variable
func initEnv() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Println("Error loading .env file")
	}

	list.CreateFiles()
}

// Initialize the in-memory list for the given level and set callback
func createCache(level model.Level) *ttlcache.Cache {
	c := ttlcache.NewCache()

	if level == model.MLDNS {
		duration, _ := time.ParseDuration("24h")
		c.SetTTL(duration)
	} else {
		c.SetTTL(ttlcache.ItemNotExpire)
	}

	list.LoadListIntoCache(c, level, model.Allowed)
	list.LoadListIntoCache(c, level, model.Blocked)

	c.SetExpirationCallback(func(key string, value interface{}) {
		fmt.Printf("Domain(%s) with status (%d) expired for level (%d) \n", key, value, level)
	})

	c.SetNewItemCallback(func(key string, value interface{}) {
		fmt.Printf("New domain(%s) added with status (%d) for level (%d)\n", key, value, level)
		domain := model.Domain{
			Domain:    key,
			Status:    value.(model.Status),
			Timestamp: time.Now(),
			Level:     level,
		}
		list.WriteToFile(domain)
	})

	return c
}
