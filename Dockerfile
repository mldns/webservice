## We specify the base image we need for our
## go application
FROM golang:1.17.1-alpine3.14
# Add unbound to use unbound-control
RUN apk add unbound
## We create an /app directory within our
## image that will hold our application source
## files
RUN mkdir /app
## We copy everything in the root directory
## into our /app directory
ADD . /app
## We specify that we now wish to execute
## any further commands inside our /app
## directory
WORKDIR /app
## we run go build to compile the binary
## executable of our Go program
RUN go build -o main .

EXPOSE 80
EXPOSE 443
EXPOSE 8080

ADD .env.dist /app/.env
ADD config.json /app/config/config.json
ADD unbound.conf /etc/unbound/unbound.conf

## Our start command which kicks off
## our newly created binary executable
CMD ["/app/main"]
