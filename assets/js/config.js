window.addEventListener('DOMContentLoaded', (event) => {
    api.getCall(api.baseUrl + '/api/config')
        .then((data) => {
            console.log(data);
            document.getElementById("toggleSwitchCheckbox").checked = data.active;
            document.getElementById("myRange").value = (data.threshold * 1000).toString();
            document.getElementById("sendDataAnonymCheckbox").checked = data.anonymous_data;
        })
        .catch((e) => {
            console.error(e);
        });

    const saveButton = document.getElementById("saveButton");
    saveButton.addEventListener("click", function (event) {
        const threshold = Number(document.getElementById("myRange").value) / 1000;
        const active = document.getElementById("toggleSwitchCheckbox").checked;
        const anonymous_data = document.getElementById("sendDataAnonymCheckbox").checked;

        const data = { active: active, threshold: threshold, anonymous_data: anonymous_data };
        const url = api.baseUrl + '/api/config';
        api.postCall(url, data)
            .then((data) => {
                console.log(data);
            })
            .catch((e) => {
                console.error(e);
            });
    });


    const deleteFilterHistoryButton = document.getElementById("deleteFilterHistoryButton");
    deleteFilterHistoryButton.addEventListener("click", function (event) {
        const url = api.baseUrl + '/api/logs';
        api.deleteCall(url)
            .then((data) => {
                console.log(data);
            })
            .catch((e) => {
                console.error(e);
            });
    });
});
