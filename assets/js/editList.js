function deleteFromList(domain, level, status, listName) {
  /* show confirmation panel */
  document.querySelector(".bg-modal").style.display = "flex";
  document.querySelector("#modelText").innerHTML =
    "Do you really want to delete <b>'" +
    domain +
    "'</b> from your " +
    listName +
    "?";

  /* handle abort button */
  document.getElementById("abort").addEventListener("click", function () {
    document.querySelector(".bg-modal").style.display = "none";
  });

  /* handle abort button cross*/
  document.querySelector(".close").addEventListener("click", function () {
    document.querySelector(".bg-modal").style.display = "none";
  });

  /* handle confirm button */
  document.getElementById("confirm").addEventListener("click", function () {
    api
      .deleteCallWithPayload(api.baseUrl + "/api/lists/domain", {
        domain: domain,
        level: level,
        status: status
      })

      .then((data) => {
        console.log(data);
        document.querySelector(".bg-modal").style.display = "none";
        window.location.reload();
      })
      .catch((e) => {
        console.error(e);
      });
  });
}

function deleteList(level, status, listName) {
  /* show confirmation panel */
  document.querySelector(".bg-modal").style.display = "flex";

  document.querySelector("#modelText").innerHTML =
    "Do you really want to delete all " + listName + " entries?";

  /* handle abort button */
  document.getElementById("abort").addEventListener("click", function () {
    document.querySelector(".bg-modal").style.display = "none";
  });

  /* handle abort button cross*/
  document.querySelector(".close").addEventListener("click", function () {
    document.querySelector(".bg-modal").style.display = "none";
  });

  /* handle confirm button */
  document.getElementById("confirm").addEventListener("click", function () {
    api
      .deleteCall(
        api.baseUrl + "/api/lists?status=" + status + "&level=" + level
      )
      .then((data) => {
        console.log(data);
        document.querySelector(".bg-modal").style.display = "none";
        window.location.reload();
      })
      .catch((e) => {
        console.error(e);
      });
  });
}

function addToList(domain, level, status, listName) {
  /* show confirmation panel */
  document.querySelector(".bg-modal").style.display = "flex";

  innerHTML = "<div>" + "<h3>Add new Entry to " + listName + "</h3>";

  if (domain != undefined) {
    innerHTML +=
      "<input id='newEntry' class='ml-input' placeholder='example.com' type='text' value='" +
      domain +
      "'>";
  } else {
    innerHTML +=
      "<input id='newEntry' class='ml-input' placeholder='example.com' type='text'>";
  }
  innerHTML += "</div>";

  document.querySelector("#modelText").innerHTML = innerHTML;

  /* handle abort button */
  document.getElementById("abort").addEventListener("click", function () {
    document.querySelector(".bg-modal").style.display = "none";
  });

  /* handle abort button cross*/
  document.querySelector(".close").addEventListener("click", function () {
    document.querySelector(".bg-modal").style.display = "none";
  });

  /* handle confirm button */
  document.getElementById("confirm").addEventListener("click", function () {
    domain = document.getElementById("newEntry").value;
    api
      .postCall(api.baseUrl + "/api/lists", {
        domain: domain,
        level: level,
        status: status,
      })
      .then((data) => {
        console.log(data);
        document.querySelector(".bg-modal").style.display = "none";
        window.location.reload();
      })
      .catch((e) => {
        console.error(e);
      });
  });
}

function goBack() {
  window.history.back();
}
