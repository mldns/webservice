const api = (function () {
    return {
        baseUrl: '',

        getCall: async (url) => {
            const response = await fetch(url)
            if (!response.ok) {
                return Promise.reject(response);
            }
            return response.json();
        },

        postCall: async (url, data) => {
            const settings = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(url, settings);
            if (!response.ok) {
                return Promise.reject(response);
            }
            return response.json();
        },

        deleteCallWithPayload: async (url, data) => {
            const settings = {
                method: "delete",
                body: JSON.stringify(data),
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(url, settings);
            if (!response.ok) {
                return Promise.reject(response);
            }
            return response;
        },

        deleteCall: async (url) => {
            const settings = {
                method: "delete",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
            };
            const response = await fetch(url, settings);
            if (!response.ok) {
                return Promise.reject(response);
            }
            return response;
        },
    };
})();
