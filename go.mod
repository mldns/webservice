module gitlab.com/mldns/webservice

go 1.16

require (
	github.com/ReneKroon/ttlcache/v2 v2.8.1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/joho/godotenv v1.3.0
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/tools v0.1.3 // indirect
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.25.0
)
